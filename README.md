# Alert Rules in Prometheus Part 2

#### Project Outline

In the previous project we configured the alert rules, we will now trigger emails from when an alert is reached.
So to begin, we have to know that the Prometheus server and Alert Manager are both different applications and therefore having it’s own configuration.


##### Lets get started

Lets open up the Alerts Manager UI

```
kubectl get svc -n monitoring
```

![Image 1](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image1.png)

Can run the following command


```
kubectl port-forward svc/prometheus-monitoring-kube-alertmanager -n monitoring 9093:9093 &
```

![Image 2](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image2.png)

Can now access the UI

![Image 3](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image3.png)

And can see the alert manager configuration

![Image 4](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image4.png)

But more importantly the receivers

This configured the notification channel that are coming in from Prometheus and where we add the email configuration

![Image 5](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image5.png)

And another, thing, we need to configure which alerts go where

Now we want these two alerts to be sent to email

Note the firing alert is from the cpu test

![Image 6](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image6.png)

So now lets see where this alerts manager config comes from in the cluster

If we go back to the generated YAML’s

We can see that the alert manager configuration points towards config-volume and can see it in the below yaml file and can see it is a secret

![Image 7](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image7.png)

Can also check it aswell using the below

```
kubectl get secret alertmanager-prometheus-monitoring-kube-alertmanager-generated -n monitoring -o yaml | less
```

![Image 8](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image8.png)

Contents of the file

![Image 9](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image9.png)

Lets now decode it using base64

```
echo <base64-encoded-string> | base64 -d | gunzip  | less
```

![Image 10](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image10.png)

Can then see the results which is the config file

![Image 11](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image11.png)

Issue is we still can’t modify this

Similar to how we configured the alert rules

Can use the custom component to configure and adjust  we can navigate to the RedHat page

![Image 12](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image12.png)

Can start off the configuration like the below with the latest version and in the namespace

![Image 13](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image13.png)

The below has been configured

![Image 14](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image14.png)

Can then navigate to the receivers part

![Image 15](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image15.png)

Can also see the emailConfigs

![Image 16](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image16.png)

Can then configure the below

![Image 17](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image17.png)

Now we need to implement the SMTP protocol

Using the below

![Image 18](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image18.png)

https://developers.google.com/gmail/imap/imap-smtp#:~:text=The%20outgoing%20SMTP%20server%2C%20smtp,port%20587%20(for%20TLS)

Can then do the below

![Image 19](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image19.png)

Next we need to configure the access

For the password we can configure the secret as a key or name


![Image 20](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image20.png)

Can then do the below

![Image 21](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image21.png)

And create a file for password

Can then insert the password from App password

![Image 22](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image22.png)

And then create an email secret yaml so that we don’t have to hard code the password if we check it into a repository

![Image 23](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image23.png)

Can also include auth-identity

![Image 24](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image24.png)

Now that we have configured the email receiving end, now we need to configure the route section

The alert manager will be able to login and send an email from the devised account

Now we need to configure the route section

Going back to the documentation we can see the below

![Image 25](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image25.png)

And here we see the different attributes

![Image 26](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image26.png)

Then configure the child routes

![Image 27](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image27.png)

And then using the matchers and name

![Image 28](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image28.png)

This way we are matching both of our alerts

![Image 29](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image29.png)

Can also define the below underneath route to the receiver

![Image 30](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image30.png)

And then configure it to a receiver with ‘email’ defined and repeat Interval. Can also override and implement repeat for certain alerts like ‘KubernetesPodCrashLooping’

![Image 31](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image31.png)

Now we have our progress alert-manager-configuration.yaml and email.yaml files

![Image 32](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image32.png)

![Image 33](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image33.png)

Now ready to apply both files

```
kubectl apply -f email-secret.yaml, kubectl apply -f alert-manager-configuration.yaml
```

![Image 34](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image34.png)

Can also check the config has been deployed

```
kubectl get alertmanagerconfig -n monitoring
```

![Image 35](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image35.png)

Can see in the logs also that it got triggered aswell

![Image 36](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image36.png)

Going back to the alerts manager can see our configuration getting merged

![Image 37](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image37.png)

Now lets test it

Deleting the old load and restarting the new one

![Image 38](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image38.png)

Can then see it is in a firing state

![Image 39](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image39.png)

The below shows us in a json format all the alerts that have been fired

```
http://127.0.0.1:9093/api/v2/alerts
```

![Image 40](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image40.png)

Can see the alert email below

![Image 41](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image41.png)

Can see the restart has gone over 5 for the cpu-test since it has timed out

![Image 42](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image42.png)

And checking the alerts page

Can see it is firing for the crashbackloop

![Image 43](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image43.png)

And can see the email has been sent

![Image 44](https://gitlab.com/FM1995/alert-manager-in-prometheus/-/raw/main/Images/Image44.png)









